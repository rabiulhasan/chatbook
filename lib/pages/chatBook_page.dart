import 'dart:ffi';
import 'dart:io';

import 'package:chatbook/controller/chatBook_controller.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';

FirebaseFirestore? _firebaseFirestore;
User? _user;

class ChatBookPage extends StatefulWidget {
  const ChatBookPage({Key? key}) : super(key: key);

  @override
  State<ChatBookPage> createState() => _ChatBookPageState();
}

class _ChatBookPageState extends State<ChatBookPage> {
  TextEditingController? messageTxtClt;
  FirebaseAuth? firebaseAuth;
  FirebaseStorage? _firebaseStorage;
  ChatBookController? _chatBookController;

  @override
  void initState() {
    super.initState();
    messageTxtClt = TextEditingController();
    firebaseAuth = FirebaseAuth.instance;
    _firebaseFirestore = FirebaseFirestore.instance;
    _firebaseStorage = FirebaseStorage.instance;
    _chatBookController = ChatBookController();

    getCurrentUser();
    // print(DateTime.now());
  }

  void getCurrentUser() {
    try {
      _user = firebaseAuth!.currentUser;
    } catch (error) {
      print(error);
    }
  }

  void saveMassage(String url) {
    _firebaseFirestore!.collection("chatBook_messages").add({
      'sender': _user!.email,
      'text': messageTxtClt!.text,
      'timestamp': DateTime.now().microsecondsSinceEpoch,
      'imageUrl': url,
    });
  }

  Future<void> getImageFromCameraOrGallery(ImageSource source) async {
    final ImagePicker imagePicker = ImagePicker();
    final XFile? file = await imagePicker.pickImage(source: source);

    String fileName =
        'photo_${DateTime.now().microsecondsSinceEpoch.toString()}.jpg';

    Reference reference =
        _firebaseStorage!.ref().child('chatBook_photos').child(fileName);
    await uploadFileInFireBaseStorage(file!, reference);
  }

  Future<void> uploadFileInFireBaseStorage(
      XFile file, Reference reference) async {
    File imageFile = File(file.path);
    await reference.putFile(imageFile);
    await downloadUrlFromFireBaseStorage(reference);
  }

  Future<void> downloadUrlFromFireBaseStorage(Reference reference) async {
    String url = await reference.getDownloadURL();

    saveMassage(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Colors.grey.shade300,

      appBar: AppBar(
        title: Text('${_user!.email}'),
        leading: null,
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.close),
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            MessageStream(),
            Container(
              decoration: const BoxDecoration(),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextField(
                        controller: messageTxtClt,
                        // minLines: 2,
                        style: const TextStyle(
                          fontSize: 20,
                        ),
                        decoration: InputDecoration(
                            prefixIcon: IconButton(
                              onPressed: () async {
                                await getImageFromCameraOrGallery(ImageSource.gallery);
                              },
                              icon: const Icon(Icons.camera),
                            ),
                            suffixIcon: IconButton(
                              onPressed: () async {
                                 await getImageFromCameraOrGallery(ImageSource.camera);

                              },
                              icon: const Icon(Icons.camera_alt),
                            ),
                            hintText: "Type Message..",
                            filled: true,
                            // fillColor: Colors.grey.shade300,
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 10, horizontal: 20),
                            // border: InputBorder.none,
                            border: const OutlineInputBorder(
                                borderSide: BorderSide(),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(30)))),
                        onChanged: (value) {},
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      saveMassage("no-image");
                      messageTxtClt!.clear();
                    },
                    icon: const Icon(Icons.send_rounded),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class MessageStream extends StatelessWidget {
  const MessageStream({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firebaseFirestore!
          .collection("chatBook_messages")
          .orderBy('timestamp', descending: true)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<MessageBubble> messageWidgetList = [];
          for (QueryDocumentSnapshot querySnapshot in snapshot.data!.docs) {
            String mMessage = querySnapshot.get('text');
            String mSender = querySnapshot.get('sender');
            int mTime = querySnapshot.get('timestamp');
            String mImageUrl = querySnapshot.get('imageUrl');

            int year = DateTime.fromMicrosecondsSinceEpoch(mTime).year.toInt();
            DateTime date = DateTime.fromMicrosecondsSinceEpoch(mTime);
            //String mDate = DateFormat(DateFormat.MONTH_DAY).format(date).toString();
            String mDate = "";
            if (year == DateTime.now().year) {
              mDate = DateFormat('MMM dd, kk:mm a').format(date).toString();
            } else {
              mDate =
                  DateFormat('yyyy,MMM dd, kk:mm a').format(date).toString();
            }

            MessageBubble messageBubble = MessageBubble(
              isUserMe: mSender == _user!.email ? true : false,
              msgSender: mSender,
              msxText: mMessage,
              time: mDate,
              imageUrl: mImageUrl,
            );
            messageWidgetList.add(messageBubble);
          }
          return Expanded(
            child: ListView(
              reverse: true,
              children: messageWidgetList,
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class MessageBubble extends StatelessWidget {
  final String? msxText;
  final String? msgSender;
  final String? time;
  final String? imageUrl;
  final bool? isUserMe;

  const MessageBubble(
      {this.isUserMe,
      this.msgSender,
      this.msxText,
      this.time,
      this.imageUrl,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: isUserMe == true
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        children: [
          Text(
            msgSender!,
            style: const TextStyle(
              fontSize: 12,
              //color: Colors.white,
            ),
          ),
          imageUrl == 'no-image'
              ? Material(
                  color: isUserMe == true ? Colors.blue : Colors.lime.shade300,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    bottomRight: Radius.circular(50),
                    topLeft: isUserMe == true
                        ? Radius.circular(50)
                        : Radius.circular(0),
                    topRight: isUserMe == true
                        ? Radius.circular(0)
                        : Radius.circular(50),
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 20,
                        ),
                        child: Text(
                          msxText!,
                          style: TextStyle(
                            fontSize: 20,
                            color:
                                isUserMe == true ? Colors.white : Colors.blue,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 20,
                        ),
                        child: Text(
                          time!.toString(),
                          style: const TextStyle(
                            fontSize: 10,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Container(
                  height: 200,
                  width: 200,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Image.network(
                    imageUrl!,
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                  ),
                ),
        ],
      ),
    );
  }
}
