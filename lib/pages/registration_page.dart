import 'package:chatbook/pages/chatBook_page.dart';
import 'package:chatbook/pages/profile_photoUpload_page.dart';
import 'package:chatbook/pages/userList_page.dart';
import 'package:chatbook/pages/widgets/custom_button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

FirebaseFirestore? _firebaseFirestore;
User? user;
class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  TextEditingController? _nameTxtClt;
  TextEditingController? _emailTxtClt;
  TextEditingController? _passwordTxtClt;
  TextEditingController? _passwordConTxtClt;
  FirebaseAuth? _firebaseAuth;

  @override
  void initState() {
    super.initState();
    _nameTxtClt = TextEditingController();
    _emailTxtClt = TextEditingController();
    _passwordTxtClt = TextEditingController();
    _passwordConTxtClt = TextEditingController();
    _firebaseAuth = FirebaseAuth.instance;
    _firebaseFirestore = FirebaseFirestore.instance;
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
            child: Text(
          'Sign Up',
          style: TextStyle(color: Colors.black),
        )),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: const EdgeInsets.symmetric(vertical: 15, horizontal: 15),
          padding: const EdgeInsets.only(
            top: 10,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    'Create New Account',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Hero(
                tag: "Logo-tag",
                child: SizedBox(
                  height: 100,
                  child: Image.asset('assets/images/logo.png'),
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Row(
                children: const [
                  Text(
                    'Full  Name',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                controller: _nameTxtClt,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.shade300,
                  border: InputBorder.none,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                children: const [
                  Text(
                    'Email Address',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                controller: _emailTxtClt,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.shade300,
                  border: InputBorder.none,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                children: const [
                  Text(
                    'Password',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                controller: _passwordTxtClt,
                obscureText: true,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.shade300,
                  border: InputBorder.none,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Row(
                children: const [
                  Text(
                    'Confirm Password',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    ' *',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                controller: _passwordConTxtClt,
                obscureText: true,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.grey.shade300,
                  border: InputBorder.none,
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              CustomButton(
                onPressed: () async {
                  try {
                    if (_emailTxtClt!.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(
                         const SnackBar(content: Text("Enter Your valid email")));
                    } else if (_passwordTxtClt!.text.isEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(
                         const SnackBar(content: Text("Enter Password")));
                    } else {
                      await _firebaseAuth!.createUserWithEmailAndPassword(
                        email: _emailTxtClt!.text,
                        password: _passwordTxtClt!.text,
                      );
                    await Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return  ProfilePhotoUpload();
                      }));
                    }
                    // Navigator.push(context,
                    //          MaterialPageRoute(builder: (context) {
                    //         return const UserListPage();
                    //       }));
                  } catch (er) {
                    ScaffoldMessenger.of(context)
                        .showSnackBar(SnackBar(content: Text('$er')));
                  }
                },
                title: "Contiune",
              )
            ],
          ),
        ),
      ),
    );
  }
}



