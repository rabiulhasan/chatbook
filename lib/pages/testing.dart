import 'dart:convert';
import 'dart:io';

import 'package:chatbook/pages/widgets/custom_button.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class TestingPage extends StatefulWidget {
  const TestingPage({Key? key}) : super(key: key);

  @override
  State<TestingPage> createState() => _TestingPageState();
}

class _TestingPageState extends State<TestingPage> {
  File? _imageFile;
  final picker = ImagePicker();

  Future pickImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _imageFile = File(pickedFile!.path);
    });

    // Future uploadImageToFirebase(BuildContext context) async {
    //   String fileName = basename(_imageFile!.path);
    //   Reference firebaseStorageRef =
    //   FirebaseStorage.instance.ref().child('uploads/$fileName');
    //   UploadTask uploadTask = firebaseStorageRef.putFile(_imageFile!);
    //   TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
    //   taskSnapshot.ref.getDownloadURL().then(
    //         (value) => print("Done: $value"),
    //   );
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: 360,
            decoration: BoxDecoration(
              color: Colors.greenAccent,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(50.0),
                bottomRight: Radius.circular(50.0),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 80),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      "Upload Your Profile Picture",
                      style: TextStyle(
                          //color: Colors.black,
                          fontSize: 28,
                          fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                SizedBox(height: 20.0),
                Expanded(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: 300,
                        width: 250,
                        decoration: BoxDecoration(
                            color: Colors.lightBlueAccent,
                            borderRadius: BorderRadius.circular(15),
                            border: Border.all(width: 5)),
                        margin: const EdgeInsets.only(
                            left: 30.0, right: 30.0, top: 10.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: _imageFile != null
                              ? Image.file(_imageFile!,
                                  fit: BoxFit.cover,
                                  alignment: Alignment.center)
                              : FlatButton(
                                  child: Icon(
                                    Icons.add_a_photo,
                                    size: 50,
                                  ),
                                  onPressed: pickImage,
                                ),
                        ),
                      ),
                    ],
                  ),
                ),
                uploadImageButton(context),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget uploadImageButton(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              child: CustomButton(
                onPressed: () {
                   uploadImageButton(context);
                },
                title: 'Upload Image',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              child: CustomButton(
                onPressed: () {
                  setState((){
                    _imageFile=="Null";
                  });

                },
                title: 'Skip',
              ),
            ),
          ),
        ],
      ),
    );
  }
}
