import 'package:chatbook/pages/chatBook_page.dart';
import 'package:chatbook/pages/registration_page.dart';
import 'package:chatbook/pages/userList_page.dart';
import 'package:chatbook/pages/widgets/custom_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController? _emailTxtClt;
  TextEditingController? _passwordTxtClt;
  FirebaseAuth? firebaseAuth;
  bool? showSpinner;

  @override
  void initState() {
    super.initState();
    _emailTxtClt = TextEditingController();
    _passwordTxtClt = TextEditingController();
    firebaseAuth = FirebaseAuth.instance;
    showSpinner = false;
  }

  void loginHelper() async {
    if (_emailTxtClt!.text.isEmpty) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text("Enter your email address!")));
    } else if (_passwordTxtClt!.text.isEmpty) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Enter your correct password!")));
    } else {
      setState(() {
        showSpinner = true;
      });
      try {
        UserCredential userCredential =
            await firebaseAuth!.signInWithEmailAndPassword(
          email: _emailTxtClt!.text,
          password: _passwordTxtClt!.text,
        );
        if (userCredential.user!.email == _emailTxtClt!.text) {
          setState(() {
            showSpinner = false;
          });
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return const UserListPage();
          }));
        } else {
          setState(() {
            showSpinner = false;
          });
        }
      } catch (er) {
        setState(() {
          showSpinner = false;
        });
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text('$er')));
        //print(er);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return showSpinner == false
        ? Scaffold(
            appBar: AppBar(
              title: const Center(child: Text('Log in page')),
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(Icons.arrow_back),
              ),
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Container(
                  margin: const EdgeInsets.only(top: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 150,
                        child: Image.asset("assets/images/logo.png"),
                      ),
                      const SizedBox(
                        height: 50,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: TextField(
                          controller: _emailTxtClt,
                          style: const TextStyle(fontSize: 20),
                          decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.email_outlined),
                            hintText: 'Email',
                            filled: true,
                            fillColor: Colors.grey.shade300,
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            border: const OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.blue),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: TextField(
                          controller: _passwordTxtClt,
                          obscureText: true,
                          style: const TextStyle(fontSize: 20),
                          decoration: InputDecoration(
                            prefixIcon: const Icon(Icons.password_outlined),
                            hintText: 'Password',
                            filled: true,
                            fillColor: Colors.grey.shade300,
                            enabledBorder: const OutlineInputBorder(
                              borderSide: BorderSide(
                                color: Colors.grey,
                              ),
                            ),
                            border: const OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue)),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      const Text('Forget Password?',
                          style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          )),
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: CustomButton(
                          onPressed: () {
                            loginHelper();
                          },
                          title: "Login",
                        ),
                      ),
                      const SizedBox(
                        height: 30,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15, right: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Text(
                              "Don't have an Account? ",
                              style: TextStyle(fontSize: 20),
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.push(context,
                                      MaterialPageRoute(builder: (context) {
                                    return const RegistrationPage();
                                  }));
                                },
                                child: const Text(
                                  "Sign up",
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.blue,
                                  ),
                                )),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        : const Center(
            child: CircularProgressIndicator(
              color: Colors.blue,
            ),
          );
  }
}
