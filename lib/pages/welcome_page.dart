import 'package:chatbook/pages/login_page.dart';
import 'package:chatbook/pages/registration_page.dart';
import 'package:chatbook/pages/userList_page.dart';
import 'package:chatbook/pages/widgets/custom_button.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage>
    with SingleTickerProviderStateMixin {
  AnimationController? animationController;

  @override
  void initState() {
    super.initState();
    // animationController = AnimationController(
    //   vsync: this,
    //   duration: const Duration(seconds: 5),
    //   //upperBound: 100,
    // );
    // animationController!.forward();
    // animationController!.addListener(() {
    //   setState(() {});
    //  // print(animationController!.value);
    // });
  }

  @override
  void dispose() {
    super.dispose();
    animationController!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   leading: null,
      // ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Welcome',
              style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Hero(
                  tag: "Logo-tag",
                  child: SizedBox(
                    height: 60,
                    //height: animationController!.value,
                    child: Image.asset('assets/images/logo.png'),
                  ),
                ),
                Text(
                  'ChatBook',
                  //'${ animationController!.value.toInt()}%',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 60,
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: CustomButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return const LoginPage();
                  }));
                },
                title: "Login",
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: CustomButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return const RegistrationPage();
                  }));
                },
                title: "Registration",
              ),
            ),
            // Padding(
            //   padding: const EdgeInsets.all(15.0),
            //   child: CustomButton(
            //     onPressed: () {
            //       Navigator.push(context, MaterialPageRoute(builder: (context) {
            //         return const UserListPage();
            //       }));
            //     },
            //     title: "CHATS",
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
