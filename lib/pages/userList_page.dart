import 'package:chatbook/pages/chatBook_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

FirebaseFirestore? _firebaseFirestore;
User? user;

class UserListPage extends StatefulWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  FirebaseAuth? firebaseAuth;

  @override
  void initState() {
    super.initState();
    _firebaseFirestore = FirebaseFirestore.instance;
    firebaseAuth = FirebaseAuth.instance;
    getCurrentUser();
  }

  void getCurrentUser() {
    try {
      user = firebaseAuth!.currentUser;
      //print(_user!.email);
    } catch (error) {
      print(error);
    }
  }

  final List<String> entries = <String>['A', 'B', 'C'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('CHATS'),
      ),
      floatingActionButton: Container(
        margin: const EdgeInsets.only(right: 30, bottom: 30),
        child: FloatingActionButton(
          onPressed: () async {
            await Navigator.push(context, MaterialPageRoute(builder: (context){
              return ChatBookPage();
            }));
          },
          child: const Icon(Icons.message),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              decoration: InputDecoration(
                hintText: 'Search here user name...',
                suffixIcon: Icon(Icons.search),
                filled: true,
                fillColor: Colors.grey.shade300,
                border: InputBorder.none,
              ),
            ),
            MessageStream(),
            // Expanded(
            //   child: ListView.separated(
            //     separatorBuilder: (context, index) {
            //       return SizedBox(
            //         height: 10,
            //       );
            //     },
            //     itemCount: entries.length,
            //     itemBuilder: (context, index) {
            //       return Container(
            //         height: 50,
            //         color: Colors.grey.shade300,
            //         child: Center(
            //           child: Text(' ${user!.email}'),
            //         ),
            //       );
            //     },
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}

class MessageStream extends StatelessWidget {
  const MessageStream({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firebaseFirestore!
          .collection("chatBook_messages")
          .orderBy('timestamp', descending: true)
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<MessageBubble> messageWidgetList = [];
          for (QueryDocumentSnapshot querySnapshot in snapshot.data!.docs) {
            String mSender = querySnapshot.get('sender');
            //String mImageUrl = querySnapshot.get('imageUrl');

            MessageBubble messageBubble = MessageBubble(
              // isUserMe: mSender == user!.email ? true : false,
              //imageUrl: mImageUrl,
              msgSender: mSender,
            );
            messageWidgetList.add(messageBubble);
          }
          return Expanded(
            child: ListView(
              reverse: true,
              children: messageWidgetList,
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class MessageBubble extends StatelessWidget {
  final String? imageUrl;
  final String? msgSender;
  final bool? isUserMe;

  const MessageBubble({this.isUserMe, this.msgSender, this.imageUrl, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        // crossAxisAlignment:CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              imageUrl == 'no-image'
                  ? Container(
                      height: 50,
                      width: 50,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Image.network(
                        imageUrl!,
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                      ),
                    )
                  : Container(
                      height: 50,
                      width: 50,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(16),
                      ),
                      child: Image.asset(
                        'assets/images/missing_avatar.png',
                        fit: BoxFit.cover,
                        alignment: Alignment.center,
                      ),
                    ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 10,
                  horizontal: 20,
                ),
                child: Text(
                  msgSender!,
                  style: TextStyle(
                    fontSize: 20,
                    color: isUserMe == true ? Colors.white : Colors.blue,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
