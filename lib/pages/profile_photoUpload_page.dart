import 'dart:io';

import 'package:chatbook/pages/chatBook_page.dart';
import 'package:chatbook/pages/widgets/custom_button.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

FirebaseStorage? _firebaseStorage;
FirebaseFirestore? _firebaseFirestore;
FirebaseAuth? firebaseAuth;
User? user;

class ProfilePhotoUpload extends StatefulWidget {
  const ProfilePhotoUpload({Key? key}) : super(key: key);

  @override
  State<ProfilePhotoUpload> createState() => _ProfilePhotoUploadState();
}

class _ProfilePhotoUploadState extends State<ProfilePhotoUpload> {
  @override
  void initState() {
    super.initState();
    firebaseAuth = FirebaseAuth.instance;
    _firebaseFirestore = FirebaseFirestore.instance;
    _firebaseStorage = FirebaseStorage.instance;
    getCurrentUser();
  }

  void getCurrentUser() {
    try {
      user = firebaseAuth!.currentUser;
    } catch (error) {
      print(error);
    }
  }

  void saveCustomUser(String url) {
    _firebaseFirestore!.collection("userProfile").add({
      'userId':user!.uid,
      'imageUrl': url,
      'userName': 'name',
    });
  }


  Future<void> getImageFromCameraOrGallery(ImageSource source) async {
    final ImagePicker imagePicker = ImagePicker();
    final XFile? file = await imagePicker.pickImage(source: source);

    String fileName = 'photo_${user!.uid.toString()}.jpg';

    Reference reference =
        _firebaseStorage!.ref().child('userProfile_photos').child(fileName);
    await uploadFileInFireBaseStorage(file!, reference);
  }

  Future<void> uploadFileInFireBaseStorage(
      XFile file, Reference reference) async {
    File imageFile = File(file.path);
    await reference.putFile(imageFile);
    await downloadUrlFromFireBaseStorage(reference);
  }

  Future<void> downloadUrlFromFireBaseStorage(Reference reference) async {
    String url = await reference.getDownloadURL();
    saveCustomUser(url);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile Photo'),
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children:[
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
              child: Container(
                //height: 300,
              //  width: 300,

                // decoration: const BoxDecoration(
                //   //color: Theme.of(context).primaryColor,
                //   color: Colors.purple,
                //   borderRadius: BorderRadius.only(
                //     bottomLeft: Radius.circular(20.0),
                //     bottomRight: Radius.circular(20.0),
                //   ),
                // ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomUser(),
                    Text("Hi ${user!.email ?? 'nice to see you here.'}",style: TextStyle(color: Colors.black),),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: CustomButton(
                onPressed: () async{
                 await getImageFromCameraOrGallery(ImageSource.camera);
                },
                title: 'Upload',
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: CustomButton(
                onPressed: () {
                  Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) {
                    return const ChatBookPage();
                  }));
                },
                title: 'Skip',
              ),
            ),

          ],
        ),
      ),
    );
  }

  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Text('data'),
  //     ),
  //     body: SafeArea(
  //       child: Column(
  //         crossAxisAlignment: CrossAxisAlignment.stretch,
  //         children: <Widget>[
  //           Padding(
  //             padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
  //             child: Container(
  //               height: 300,
  //               width: 300,
  //               decoration: const BoxDecoration(
  //                 //color: Theme.of(context).primaryColor,
  //                 color: Colors.purple,
  //                 borderRadius: BorderRadius.only(
  //                   bottomLeft: Radius.circular(20.0),
  //                   bottomRight: Radius.circular(20.0),
  //                 ),
  //               ),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 children: <Widget>[
  //                   Avatar(
  //                     // avatarUrl: _currentUser.avatarUrl,
  //
  //                     onTap: () async {
  //                       await getImageFromCameraOrGallery(ImageSource.camera);
  //                     },
  //                   ),
  //                   Text("Hi ${user!.email ?? 'nice to see you here.'}"),
  //                 ],
  //               ),
  //             ),
  //           ),
  //           const SizedBox(
  //             height: 20,
  //           ),
  //           Padding(
  //             padding: const EdgeInsets.all(10.0),
  //             child: CustomButton(
  //               onPressed: () {},
  //               title: 'Upload',
  //             ),
  //           ),
  //           Padding(
  //             padding: const EdgeInsets.all(10.0),
  //             child: CustomButton(
  //               onPressed: () {
  //                 Navigator.pushReplacement(context,
  //                     MaterialPageRoute(builder: (context) {
  //                       return const ChatBookPage();
  //                     }));
  //               },
  //               title: 'Skip',
  //             ),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
}



class Avatar extends StatelessWidget {
  final String? avatarUrl;
  final Function()? onTap;

  const Avatar({
    this.avatarUrl,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Center(
        child: avatarUrl == null
            ? const CircleAvatar(
                radius: 50.0,
                child: Icon(Icons.photo_camera),
              )
            : CircleAvatar(
                radius: 50.0,
                backgroundImage: NetworkImage(avatarUrl!),
              ),
      ),
    );
  }
}

class MessageStream extends StatelessWidget {
  const MessageStream({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _firebaseFirestore!
          .collection("profile_photos")
          .snapshots(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<CustomUser> profileWidgetList = [];
          for (QueryDocumentSnapshot querySnapshot in snapshot.data!.docs) {
            String mUserId = querySnapshot.get('userId');
            String mImageUrl = querySnapshot.get('imageUrl');

            CustomUser customUser = CustomUser(
             // isUserMe: mSender == user!.email ? true : false,
              userId: mUserId,
              imageUrl: mImageUrl,
            );
            profileWidgetList.add(customUser);
          }
          return Expanded(
            child: ListView(
              reverse: true,
              children: profileWidgetList,
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}
class CustomUser extends StatelessWidget {
  final String? imageUrl;
  final String? userId;
  final String? userName;
  final bool? isUserMe;

  const CustomUser({this.isUserMe, this.userId, this.imageUrl,this.userName, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: isUserMe == true
            ? CrossAxisAlignment.end
            : CrossAxisAlignment.start,
        children: [
          imageUrl == 'no-image'
              ? Container(
                  height: 200,
                  width: 200,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Image.network(
                    imageUrl!,
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                  ),
                )
              : Container(
                  height: 200,
                  width: 200,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Image.asset(
                    'assets/images/missing_avatar.png',
                    fit: BoxFit.cover,
                    alignment: Alignment.center,
                  ),
                ),
        ],
      ),
    );
  }
}
