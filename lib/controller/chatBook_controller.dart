import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';

class ChatBookController{

  FirebaseFirestore? _firebaseFirestore;

  FirebaseStorage? _firebaseStorage;


  Future<void> getImageFromCameraOrGallery(ImageSource source) async {
    final ImagePicker imagePicker = ImagePicker();
    final XFile? file = await imagePicker.pickImage(source: source);

    String fileName =
        'photo_${DateTime.now().microsecondsSinceEpoch.toString()}.jpg';

    Reference reference =
    _firebaseStorage!.ref().child('chatBook_photos').child(fileName);
    await uploadFileInFireBaseStorage(file!, reference);
  }

  Future<void> uploadFileInFireBaseStorage(
      XFile file, Reference reference) async {
    File imageFile = File(file.path);
    await reference.putFile(imageFile);
    await downloadUrlFromFireBaseStorage(reference);
  }

  Future<String> downloadUrlFromFireBaseStorage(Reference reference) async {
    String url = await reference.getDownloadURL();
    return url;
  }
}