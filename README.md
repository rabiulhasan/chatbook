# chatbook

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
## Task
1. Welcome page
2. Sign Up Page
3. Login Page
4. Hero tag
5. Animation simple

## firebase database create
1. console.firebase -google url
2. Add project -continue
3. Adding firebase to app -android
4. Register app
    - android package name
    - app name optional
5. Download config
    - download config and put it android-app folder
6. Add firebase Sdk
    -follow instruction
7. Next
8. Console

## Connection setup with firebase database
--pub.dev
1. Firebase core 
2. Firebase auth
3. cloud firestore 
4. firebase storage

    